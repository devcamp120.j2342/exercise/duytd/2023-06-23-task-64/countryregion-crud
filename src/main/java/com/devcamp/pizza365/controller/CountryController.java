package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.repository.CountryRepository;
import com.devcamp.pizza365.service.CountryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class CountryController {
	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	CountryService countryService;

	@CrossOrigin
	@PostMapping("/country/create")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry cCountry) {
		try {
			// CCountry newRole = new CCountry();
			// newRole.setCountryName(cCountry.getCountryName());
			// newRole.setCountryCode(cCountry.getCountryCode());
			// newRole.setRegions(cCountry.getRegions());
			// CCountry savedRole = countryRepository.save(newRole);
			return new ResponseEntity<>(countryService.createCountry(cCountry) , HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Voucher: "+e.getCause().getCause().getMessage());
		}
	}

	@CrossOrigin
	@PutMapping("/country/update/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody CCountry cCountry) {
		return countryService.updateCountryService(id, cCountry);
	}

	@CrossOrigin
	@DeleteMapping("/country/delete/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		return countryService.deleteCountry(id);
	}

	@CrossOrigin
	@DeleteMapping("/delete/all")
	public ResponseEntity<Object> deleteCountryById() {
		try {
			countryRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/country/details/{id}")
	public CCountry getCountryById(@PathVariable Long id) {
		return countryService.getCountryByIdSV(id);
	}

	@CrossOrigin
	@GetMapping("/country/all")
	public List<CCountry> getAllCountry() {
		return countryRepository.findAll();
	}
}
