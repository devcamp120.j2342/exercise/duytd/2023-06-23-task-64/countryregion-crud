package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.repository.CountryRepository;
import com.devcamp.pizza365.repository.RegionRepository;
import com.devcamp.pizza365.service.RegionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class RegionController {
	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private RegionService regionService;

	@Autowired
	private CountryRepository countryRepository;
	
	@CrossOrigin
	@PostMapping("/region/create/{id}")
	public ResponseEntity<Object> createRegionById(@PathVariable("id") Long id, @RequestBody CRegion cRegion) {
		return regionService.createRegionService(id, cRegion);
	}

	@CrossOrigin
	@PostMapping("/create/region")
	public ResponseEntity<CRegion> createRegion(@RequestBody CRegion cRegion){
		return regionService.createRegionService(cRegion);
	}

	@CrossOrigin
	@PutMapping("/region/update/{id}")
	public ResponseEntity<Object> updateRegionById(@PathVariable("id") Long id, @RequestBody CRegion cRegion) {
		return regionService.updateRegion(id, cRegion);
	}

	@CrossOrigin
	@DeleteMapping("/region/delete/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable Long id) {
		return regionService.deleteRegion(id);
	}

	@CrossOrigin
	@GetMapping("/region/details/{id}")
	public CRegion getRegionById(@PathVariable Long id) {
		return regionService.getRegionById(id);
	}


	@CrossOrigin
	@GetMapping("/region/all")
	public List<CRegion> getAllRegion() {
		return regionService.getAllRegion();
	}

	@CrossOrigin
	@GetMapping("/country/{countryId}")
    public List < CRegion > getRegionsByCountry(@PathVariable(value = "countryId") Long countryId) {
        return regionService.getRegionByCountryId(countryId);
    }
	
	@CrossOrigin
	@GetMapping("/country/{countryId}/regions/{id}")
    public Optional<CRegion> getRegionByRegionAndCountry(@PathVariable(value = "countryId") Long countryId,@PathVariable(value = "id") Long regionId) {
        return regionRepository.findByIdAndCountryId(regionId,countryId);
    }

	@CrossOrigin
	@GetMapping("/region/countrycode")
	public List<CRegion> getRegionByCountryCode(@RequestParam(value="countryCode") String countryCode) {
		CCountry country = countryRepository.findByCountryCode(countryCode);
		List<CRegion> regions = country.getRegions();
		return regions;
	}
}

