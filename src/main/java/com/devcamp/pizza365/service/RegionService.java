package com.devcamp.pizza365.service;

import java.util.List;
import java.util.Optional;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.repository.CountryRepository;
import com.devcamp.pizza365.repository.RegionRepository;

@Service
public class RegionService {
    @Autowired
    RegionRepository regionRepository;

    @Autowired
    CountryRepository countryRepository;

    public List<CRegion> getAllRegion() {
        return regionRepository.findAll();
    }

    public List<CRegion> getRegionByCountryId(long countryId) {
        return regionRepository.findByCountryId(countryId);
    }

    public CRegion getRegionById(long id) {
        if(regionRepository.findById(id).isPresent()){
            return regionRepository.findById(id).get();
        }else {
            return null;
        }
    }

    public ResponseEntity<CRegion> createRegionService(CRegion cRegion) {
        CRegion newRole = new CRegion();
        newRole.setRegionName(cRegion.getRegionName());
        newRole.setRegionCode(cRegion.getRegionCode());
        newRole.setCountry(cRegion.getCountry());
        CRegion saveRole = regionRepository.save(newRole);
        if(saveRole != null) {
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> updateRegion (long id, CRegion cRegion) {
        Optional<CRegion> regionData = regionRepository.findById(id);
        if (regionData.isPresent()) {
			CRegion newRegion = regionData.get();
			newRegion.setRegionName(cRegion.getRegionName());
			newRegion.setRegionCode(cRegion.getRegionCode());
			CRegion savedRegion = regionRepository.save(newRegion);
			return new ResponseEntity<>(savedRegion, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    }

    public ResponseEntity<Object> deleteRegion(long id) {
        try {
            regionRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch(Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> createRegionService(long id, CRegion cRegion) {
        try {
            Optional<CCountry> countryData = countryRepository.findById(id);
            if(countryData.isPresent()) {
                CRegion newRole = new CRegion();
                newRole.setRegionName(cRegion.getRegionName());
                newRole.setRegionCode(cRegion.getRegionCode());
                newRole.setCountry(cRegion.getCountry());
                
                CCountry _country = countryData.get();
                newRole.setCountry(_country);
                newRole.setCountryName(_country.getCountryName());
                
                CRegion savedRole = regionRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch(Exception e) {
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Voucher: "+e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
