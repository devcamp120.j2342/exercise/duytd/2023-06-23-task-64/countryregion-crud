package com.devcamp.pizza365.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.repository.CountryRepository;

@Service
public class CountryService {

	@Autowired
	private CountryRepository countryRepository;

	public CCountry createCountry(CCountry cCountry) {
		try {
			CCountry savedRole = countryRepository.save(cCountry);
			return savedRole;
		} catch (Exception e) {
			return null;
		}
	}

	public ResponseEntity<Object> updateCountryService(long id, CCountry cCountry) {
		Optional<CCountry> countryData = countryRepository.findById(id);
		if (countryData.isPresent()) {
			CCountry existingCountry = countryData.get();
			existingCountry.setCountryName(cCountry.getCountryName());
			existingCountry.setCountryCode(cCountry.getCountryCode());
			existingCountry.setRegions(cCountry.getRegions());
			CCountry updatedCountry = countryRepository.save(existingCountry);
			return new ResponseEntity<>(updatedCountry, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public ResponseEntity<Object> deleteCountry(long id) {
		try {
			Optional<CCountry> countryId = countryRepository.findById(id);
			if (countryId.isPresent()) {
				countryRepository.deleteById(id);
			}
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public CCountry getCountryByIdSV(long id) {
		Optional<CCountry> countryId = countryRepository.findById(id);
		if (countryId.isPresent()) {
			return countryRepository.findById(id).get();
		} else {
			return null;
		}
	}
}
